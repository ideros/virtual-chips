import React, {Component, PropTypes} from 'react';

// Player component - represents the player
class Player extends Component {
    render(){
        return (
           <li className={`player ${this.props.player.folded ? 'folded' : ''}`}>
                <div className="player-chips">{this.props.player.chips}$</div>
                <i className="player-icon material-icons tiny">account_circle</i>{this.props.player.name}
                <div className="player-bet">{this.props.player.bet || 'No Bet'}</div>
           </li>
        );
    }
}

Player.propTypes = {
    player: PropTypes.object.isRequired
}

export default Player;