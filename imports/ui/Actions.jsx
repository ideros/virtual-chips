import React, {Component, PropTypes} from 'react';

import Term from '../constant/Term'
import Action from './Action'

// Actions component - represents the current player's available options
class Actions extends Component {
    constructor(){
      super()

      this.state = {
        canCheck: false,
        requestingTie: false,
        tieRequested: false
      }
    }

    render(){
        return (
           <div className="actions z-depth-2-reverse">
           		<ul className="actions-horizontal">
                {/* Fold */}
           			<Action className="action-fold" click={this.fold.bind(this)} label="FOLD" />

                {/* Check */}
                {(() => this.props.foldOnly || this.state.canTie || this.state.canCheck && <Action className="action-safe" click={this.call.bind(this)} label="CHECK" />)()}

                {/* Call */}
                {(() => this.props.foldOnly || this.state.canCheck || <Action className="action-bet" click={this.call.bind(this)} label="CALL" />)()}

                {/* Tie */}
                {(() => {
                  if (!this.props.player.folded && this.state.canTie){
                    if (this.state.tieRequested){
                      return <li onClick={this.call.bind(this)} className="action action-tie-cancel waves-effect waves-light" onClick={this.cancelTie.bind(this)}><i className="material-icons">clear</i> UN-TIE</li>
                    } else {
                      if (this.state.requestingTie){
                        return <li onClick={this.call.bind(this)} className="action action-tie-processing waves-effect waves-light">...</li>
                      } else {
                        return <li onClick={this.call.bind(this)} className="action action-tie-request waves-effect waves-light" onClick={this.requestTie.bind(this)}><i className="material-icons">vertical_align_center</i> TIE</li>
                      }
                    }
                  }
                })()}
           		</ul>
           </div>
        );
    }

    call(){
      const currentPlayer = this.props.player
      const tablePlayers = this.props.table.players
      const otherPlayers = tablePlayers.filter(p => p.id._str != currentPlayer.id._str)
      const betToCall = Math.max.apply(this, otherPlayers.map(p => p.bet))

      if (!betToCall) return  // cannot call 0$
      if (betToCall < currentPlayer.bet && currentPlayer.bet === this.props.table.bigBlind) return // prevent big blind from calling

      if (betToCall !== currentPlayer.bet){
        const amountMissingForCall = betToCall - currentPlayer.bet
        const betAmount = Math.min(currentPlayer.chips, amountMissingForCall)

        Meteor.call('bet', this.props.table._id, currentPlayer.id, betAmount, (error, result) => {
          if (error) return console.log(error);
          this.props.onCall && this.props.onCall()
        });
      } else {
        this.props.onCall && this.props.onCall()
      }
    }

    fold(){
      const onFold = this.props.onFold

      Meteor.call('fold', this.props.table._id, this.props.player.id, (error, result) => {
        if (error){
          return console.log(error)
        }

        onFold && onFold()
      })
    }

    requestTie(){
        const onTie = this.props.onTie
        const tableId = this.props.table._id
        const playerId = this.props.player.id._str
        this.setState({requestingTie: true})

        Meteor.call('requestTie', tableId, playerId, (error, result) => {
            if (error){
                console.error(error)
            } else {
                this.setState({tieRequested: true})
                onTie && onTie()
            }

            this.setState({requestingTie: false})
        })
    }

    cancelTie(){
        Meteor.call('requestTie', this.props.player.id._str, this.props.table._id, true, (error, result)=> {
            if (error){
                console.error(error)
            } else {
                this.setState({tieRequested: false})
            }

            this.setState({requestingTie: false})
        })
    }

    componentWillReceiveProps(nextProps){
      const {table: nextTable, player: nextPlayer} = nextProps
      const tieRequests = nextTable.idsRequestingTie || []

      if (nextTable && nextPlayer){
        // Tie
        let canTie = false

        if (nextTable.players.filter(p => !p.folded).length > 1){
          if (nextTable.turn > Term.RIVER){
            // 1. end of round tie
            const topBet = Math.max.apply(this, nextTable.players.map(p => p.bet))
            const isEveryoneAllInOrCalled = nextTable.players.every(p => p.chips === 0 || p.bet === topBet || p.folded)

            canTie = isEveryoneAllInOrCalled
          } else {
            const amountOfPlayersWhoCanStillBet = nextTable.players.filter(p => p.chips > 0 && !p.folded).length

            // 2. there is only one player left in play (no more raise possible)
            if (amountOfPlayersWhoCanStillBet === 1){
              const currentTurn = nextTable.turn

              // other players were all-in in previous turns
              const wereAllInOnPreviousTurn = nextTable.players
                  .filter(p => p.chips === 0 && !p.folded)
                  .every(p => p.bet === 0)

              canTie = wereAllInOnPreviousTurn
            } else {
              // 3. everyone is all-in
              const isEveryOneAllIn = nextTable.players.every(p => p.chips === 0 || p.folded)
              canTie = isEveryOneAllIn
            }
          }
        }

        this.setState({canTie: canTie})
        this.setState({tieRequested: tieRequests.includes(nextPlayer.id._str)})

        // Check
        if (nextTable.players.length <= 1){
          this.setState({canCheck: true})
        } else if (nextTable.turn === Term.PRE_FLOP){
          // can check if big blind and noone else has raised
          const nooneElseHasRaised = !nextTable.players.some(p => p.bet > nextTable.bigBlind)
          this.setState({canCheck: nextPlayer.bigBlind && nooneElseHasRaised})
        } else {
          // can check if all players are at 0
          this.setState({canCheck: nextTable.players.every(p => !p.bet)})
        }
      }
    }
}

Actions.propTypes = {
    player: PropTypes.object,
    table: PropTypes.object,
    foldOnly: PropTypes.bool,
    onCall: PropTypes.func,
    onFold: PropTypes.func,
    onTie: PropTypes.func
}

export default Actions;