import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';

// Table component - represents a table available to join
class Table extends Component {    
    render(){
    	const amountOfPlayers = this.props.table.players ? this.props.table.players.length : 0

        return (
            <Link to={`${this.props.table._id}`} className="collection-item">
            	{this.props.table.name},&nbsp;
            	{amountOfPlayers || 'no'} {amountOfPlayers === 1 ? 'player' : 'players'}
            </Link>
        );
    }
}

Table.propTypes = {
    table: PropTypes.object.isRequired
};

export default Table;