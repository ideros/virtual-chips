import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';

import { Tables } from '../../api/tables';

class CreateTable extends Component {
    createTable(){
        const buyInCustomValue = ReactDOM.findDOMNode(this.refs.buyIn).value;
        const name = ReactDOM.findDOMNode(this.refs.name).value || 'New Table';
        const buyIn = (buyInCustomValue && parseInt(buyInCustomValue)) || 2500;
        const bigBlind = 200
        const smallBlind = 100

        Meteor.call('tables.create', {
           name,
           buyIn,
           smallBlind,
           bigBlind,
           createdAt: new Date(),
           idsRequestingTie: [],
           players: [],
           turn: 0,
           bigBlindIndex: 0
        }, (error, tableId)=> {
            if (tableId){
                this.context.router.push(tableId._str);
            } else {
                console.error(JSON.stringify(error));
            }
        });
    }

    render(){
        return (
            <div id="join" className="top-content">
                <form className="create-table">
                    <div className="row no-margin-bottom">
                        <div className="input-field col s8 offset-s2">
                            <input placeholder="e.g.: Sara's table" id="table-name" type="text" className="validate" required="" aria-required="true" ref="name" />
                            <label htmlFor="table-name" data-error="Please enter a name for your table">Name</label>
                        </div>

                        <div className="input-field col s8 offset-s2">
                            <input placeholder="2500" id="buy-in" type="number" ref="buyIn" />
                            <label htmlFor="buy-in">Starting chips</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col s12 aligner align-right">
                            <a onClick={this.props.cancel} className="waves-effect btn-flat">Cancel</a>
                            <a className="waves-effect waves-light btn-flat btn-create" onClick={this.createTable.bind(this)}>Create</a>
                        </div>
                    </div>
                </form>
            </div>
        );
    }

    componentDidMount(){
        Materialize.updateTextFields();
        $('#table-name').focus();
    }
}

CreateTable.contextTypes = {
    router: PropTypes.object
};

export default CreateTable;