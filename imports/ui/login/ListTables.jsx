import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {createContainer} from 'meteor/react-meteor-data';
import store from '/imports/vendor/store.everything.min';

import { Tables } from '../../api/tables';

import Table from './Table';
import CreateTable from './CreateTable';

/**
 * View or create a new table
 */
class ListTables extends Component {
    componentWillMount(){
        if (joinedRoom = store.get('room')){
            this.context.router.push(joinedRoom);
        }
    }

    displayCreate(){
        this.setState({'willCreate': true})
        setTimeout(()=> this.setState({'create' : true}), 600);
    }

    displayList(){
        this.setState({'create': false});
        this.setState({'willCreate': false});
        this.setState({'cancelCreate': true});
        setTimeout(()=> this.setState({'cancelCreate' : false}), 600);
    }

    list(){
        return (
            <div>
                <div className="main-header table-felt aligner align-vertical">
                    <h1 className="main-title">Virtual Chips</h1>
                    <h4 className="main-sub-title">No chips ? No problem !</h4>
                </div>
                <div className="z-depth-2-reverse">
                    <a onClick={this.displayCreate.bind(this)} className={`btn-floating btn-large waves-effect waves-light main-add ${this.state && this.state.willCreate && 'animate'} ${this.state && this.state.cancelCreate && 'rewind'}`}>
                        <i className="material-icons">add</i>
                    </a>
                    <div className="collection with-header no-margin-top">
                        <div className="collection-header"><h4>Select Table</h4></div>
                        {this.props.tables.map((table)=>(
                            <Table key={table._id} table={table} />
                        ))}
                    </div>
                </div>
            </div>
        );
    }

    render(){
        return (
            <div>
                {(() => this.state && this.state.create ? <CreateTable cancel={this.displayList.bind(this)}/> : this.list.call(this))()}
            </div>
        );
    }
}

ListTables.contextTypes = {
    router: PropTypes.object
};

ListTables.propTypes = {
    tables: PropTypes.array
};

export default createContainer(()=>{
    return {
        tables: Tables.find({}, { sort: { createdAt: -1} }).fetch()
    }
}, ListTables);