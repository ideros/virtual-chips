import React, {Component, PropTypes} from 'react';
import Player from './Player';
import Term from '../constant/Term'
import store from '/imports/vendor/store.everything.min'

// PlayersList component - represents the other players at the table
class PlayersList extends Component {
    constructor(props){
        super(props)

        const others = props.players.filter(p => p.id._str != store.get('currentPlayer').id._str)
        this.state = {
            otherPlayers: others
        }
    }

    render(){
        const otherPlayers = this.renderOtherPlayers();

        return (
        	<div className="players-list">
                {otherPlayers}

                <div className="table-pot">
                    <span className="turn">{this.getTurnName.call(this, this.props.table.turn || 0)}</span>
                    <span className="pot">Pot: {this.props.table.pot || 0} $</span>
                </div>
           	</div>
        );
    }

    renderOtherPlayers(){
        if (this.state.otherPlayers.length){
            return <ul className="no-margin-top">
                {
                    this.state
                        .otherPlayers
                        .map(player => <Player player={player} key={player.id._str} />)
                }
            </ul>
        } else {
            return <div className="no-players">No other players in this room</div>
        }
    }

    getTurnName(nextTurn){
        switch (nextTurn){
            case Term.PRE_FLOP: return 'Pre-flop'
            case Term.FLOP:     return 'Flop'
            case Term.TURN:     return 'Turn'
            case Term.RIVER:    return 'River'
            default:            return 'Round finished.'
        }
    }

    componentWillReceiveProps(nextProps){
        const nextPlayers = nextProps.players
        const nextTurn = nextProps.table && nextProps.table.turn

        // update the players
        if (nextPlayers) {
            let otherPlayers = nextPlayers.filter(p => p.id._str != store.get('currentPlayer').id._str)
            this.setState({ otherPlayers: otherPlayers })
        }
    }
}

PlayersList.propTypes = {
    players: PropTypes.array.isRequired,
    table: PropTypes.object.isRequired
}

export default PlayersList;