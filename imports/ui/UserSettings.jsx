import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';

import {Tables} from '../api/tables';

// UserSettings component - prompt the configurations for a user
class UserSettings extends Component {
    constructor(props){
      super(props)

      this.state = {
        name: props.player.name,
        chips: props.player.chips
      }
    }

    updateName(){
      const newName = ReactDOM.findDOMNode(this.refs.name).value;
      Meteor.call('tables.updatePlayerName', this.props.table._id, this.props.player.id, newName)
    }

    updateChips(){
      const chips = ReactDOM.findDOMNode(this.refs.chips).value;
      Meteor.call('tables.updatePlayerChips', this.props.table._id, this.props.player.id, chips)
    }

    componentWillReceiveProps(nextProps){
      const player = nextProps.player
      this.setState({name, chips} = player)
    }

    render(){
        return (
           <div className={`user-settings ${this.props.display ? '' : 'hidden'}`}>
                <div className="input-field col s8 offset-s2 black-text">
                    <input
                      placeholder="name"
                      type="text"
                      ref="name"
                      value={this.state.name}
                      onChange={this.updateName.bind(this)}
                      className="center"
                    />
                </div>

                <div className="input-field col s8 offset-s2 black-text">
                    <input
                      placeholder="name"
                      type="text"
                      ref="chips"
                      value={this.state.chips || 0}
                      onChange={this.updateChips.bind(this)}
                      className="center"
                    />
                </div>
                <div className='user-icon'>
                    <i className="material-icons">perm_identity</i>
                </div>
           </div>
        );
    }
}

UserSettings.propTypes = {
    table: PropTypes.object.isRequired,
    player: PropTypes.object.isRequired,
    display: PropTypes.bool.isRequired
}

export default UserSettings;