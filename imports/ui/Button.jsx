import React, { Component, PropTypes } from 'react'

class Button extends Component {
  render() {
    return <div className="button__container tooltipped" data-position="left" data-delay="0" data-tooltip={this.getTooltipMsg(this.props.player)}>
      {this.getButtonImage(this.props.player)}
    </div>
  }

  componentDidMount(){
      $('.button__container').tooltip({delay: 0});
  }

  getButtonImage({bigBlind, smallBlind, dealer}){
    const bigBlindIcon = () => <i className="bb">BB</i>
    const smallBlindIcon = () => <i className="sb">SB</i>
    const dealerIcon = () => <i className="d">D</i>

    if (bigBlind)             return <div className="button z-depth-1 big-blind">{bigBlindIcon()}</div>
    if (smallBlind && dealer) return <div className="sb-d"><div className="button z-depth-1 dealer">{dealerIcon()}</div><div className="button z-depth-1 small-blind">{smallBlindIcon()}</div></div>
    if (smallBlind)           return <div className="button z-depth-1 small-blind">{smallBlindIcon()}</div>
    if (dealer)               return <div className="button z-depth-1 dealer">{dealerIcon()}</div>
  }

  getTooltipMsg({bigBlind, smallBlind, dealer}){
    if (bigBlind)             return 'Big Blind, the minimum bet.'
    if (smallBlind && dealer) return 'Small Blind is Dealer in two-player games.'
    if (smallBlind)           return 'Small Blind, forced bet after dealer.'
    if (dealer)               return 'Dealer, the last to play.'
  }
}

Button.propTypes = { player: PropTypes.object }

export default Button;