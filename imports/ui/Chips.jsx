import React, {Component, PropTypes} from 'react';

// Chips component - represents the current player's Chips
class Chips extends Component {
  render(){
    return (
      <div>
        <div className="current-bet">
        	{(() => this.props.showdown ? <span className="chips-count">Showdown</span> : <span className="chips-count">{this.props.player.bet || 'No bet'}</span>)()}
        </div>

        {(() => this.props.showdown || <div className="chips-actions">
            <div onClick={this.bet.bind(this, 100)} className={`action-container waves-effect ${this.props.player.chips > 0 || 'hide'}`}>
              <span className="chips-increment">+</span>
            </div>

            <div onClick={this.bet.bind(this, -100)} className={`action-container waves-effect ${this.props.player.bet > 0 || 'hide'}`}>
              <span className="chips-decrement">-</span>
            </div>
          </div>
        )()}

        <div className="player-chips">
        	<span className="chips-count">{this.props.player.chips || '0'}</span>
        </div>
      </div>
    );
  }

  bet(bet){
    const player = this.props.player
    player.bet = player.bet || 0
    player.totalBet = (player.bet || 0) + bet
    if (player.chips - bet === 0) return this.allIn(...arguments)

    // update table
    Meteor.call('bet', this.props.table._id, this.props.player.id, bet, (error, result) => {
      if (error){
        console.error(error)
      } else {
        const onBet = this.props.onBet
        onBet && onBet(player.totalBet)
      }
    })
  }

  allIn(bet){
    const player = this.props.player

    Meteor.call('allIn', this.props.table._id, player.id, bet, player.totalBet, (error, result) => {
      if (error){
        console.error(error)
      } else {
        const onAllIn = this.props.onAllIn
        onAllIn && onAllIn(bet)
      }
    })
  }
}

Chips.propTypes = {
    player: PropTypes.object.isRequired,
    table: PropTypes.object.isRequired,
    showdown: PropTypes.bool,
    onAllIn: PropTypes.func,
    onBet: PropTypes.func
}

export default Chips;