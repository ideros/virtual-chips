import React from 'react'

/*
* stateless component experiment. I don't want to use
* Action everywhere because re-rendering causes the "waves-effect"
* to cancel animation. Only changing the text, for example, would allow
* the wave to gracefully finish.
*
* Usage :
* <Action className="" click={this.onCLick.bind(this)} label="" icon="" />
*/
const Action = ({click, label, className, icon}) => (
  <li className={`waves-effect waves-light action ${className}`} onClick={click}>
    {(() => icon && <i className="material-icons">{icon}</i>)()} {label}
  </li>
)

export default Action