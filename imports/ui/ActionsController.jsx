import React, {Component, PropTypes} from 'react';

import Term from '../constant/Term'
import Actions from './Actions'
import Chips from './Chips'

// ActionsController component - handles the impact of player actions on the table
class ActionsController extends Component {
    constructor(){
        super()
        this.state = { roundIsFinished: false }
    }

    render(){
        return <div className="poker-chips white-text">
            {this.props.displayUserSettings || <Chips showdown={this.props.player.folded || this.state.roundIsFinished} player={this.props.player} table={this.props.table}/>}
            <Actions
                table={this.props.table}
                player={this.props.player}
                foldOnly={this.props.player.folded || this.state.roundIsFinished}
                onCall={this.onCall.bind(this)}
                onFold={this.onFold.bind(this)}
                onTie={this.distributePotIfTie.bind(this)}
            />
        </div>
    }

    onCall(){
        var table = this.props.table
        var currentPlayer = this.props.player
        const betToCall = Math.max.apply(this, table.players.map(p => p.bet))
        var remainingPlayers = table.players.filter(p => !p.folded)

        if (table.players && table.players.length > 1){

            // verify all players have called
            if (remainingPlayers.every(p => p.bet === betToCall || !p.chips)){
                bigBlindPlayerBet = table.players[(table.bigBlindIndex) % table.players.length].bet
                smallBlindPlayerId = table.players[(table.bigBlindIndex + 1) % table.players.length].id

                // end turn UNLESS small blind is calling big blind in pre-flop OR IF big blind has already raised
                if (table.turn !== Term.PRE_FLOP || currentPlayer.id._str !== smallBlindPlayerId._str || bigBlindPlayerBet > table.bigBlind) this.endTurn()
            }
        }
    }

    onFold(){
        const table = this.props.table
        const remainingPlayers = table.players.filter(p => !p.folded)

        if (remainingPlayers.length === 1){
            let lastManRemaining = remainingPlayers[0]
            Meteor.call('tables.win', table._id, lastManRemaining.id, (error, result)=> {
                this.setState({roundIsFinished: false})
            })
        } else if (remainingPlayers.every(p => p.bet === remainingPlayers[0].bet)){
            this.endTurn()
        }

        this.distributePotIfTie()
    }

    distributePotIfTie(){
        const tableId = this.props.table._id
        const playersStillInGame = this.props.table.players.filter(p => !p.folded)

        if ((this.props.table.idsRequestingTie || []).length >= playersStillInGame.length){
            Meteor.call('tables.win', tableId, ...playersStillInGame.map(p => p.id), (error, result)=> {
                if (error) return console.error(error)
            })
        }
    }

    endTurn(){
        const table = this.props.table
        table && Meteor.call('tables.nextTurn', table._id, (error, result)=> {
            if (error) return console.error(error)
        });
    }

    componentWillReceiveProps(nextProps){
        const nextTable = nextProps.table;

        if (nextTable && nextTable.turn > Term.RIVER){
            this.setState({roundIsFinished: true})
        } else {
            this.setState({roundIsFinished: false})
        }
    }
}

ActionsController.propTypes = {
    table: PropTypes.object,
    player: PropTypes.object,
    displayUserSettings: PropTypes.bool
}

export default ActionsController;