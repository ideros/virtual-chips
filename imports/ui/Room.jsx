import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';
import {createContainer} from 'meteor/react-meteor-data';
import {Mongo} from 'meteor/mongo';
import store from '/imports/vendor/store.everything.min';

import UserSettings from './UserSettings';
import ActionsController from './ActionsController';
import PlayersList from './PlayersList';
import Button from './Button';

import { Tables } from '../api/tables';

import Term from '../constant/Term'

class Room extends Component {
    constructor(){
        super()

        let player = store.get('currentPlayer')
        const existing = !!player

        // create a player if not already existing
        if (!existing) {
            store.set('currentPlayer', player = {
                id: new Mongo.ObjectID(),
                name: 'Newcomer',
                chips: 0
            });
        }

        this.state = {
            displayUserSettings : false,
            currentPlayer: player
        };
    }

    componentDidMount(){
        $('#modal-leave').modal()
        $('#modal-hands').modal()
    }

    componentWillReceiveProps(nextProps){
        const table = nextProps.table

        if (table && table.name){
            let currentPlayer = store.get('currentPlayer')

            // ensure synchronization between session and server
            const existingPlayer = table.players.find(p => p.id._str === currentPlayer.id._str)

            if (existingPlayer){
                currentPlayer = existingPlayer
            } else {
                const currentRoom = store.get('room')
                const isReconnection = currentPlayer.room === currentRoom

                // initialize a player's properties when he first joins the table
                if (!isReconnection){
                    currentPlayer.bet = 0
                    currentPlayer.chips = table.buyIn
                    currentPlayer.folded = false
                    currentPlayer.room = currentRoom
                }

                Meteor.subscribe('joinTable', table._id, currentPlayer, {
                    onReady: isReconnection ? Function.prototype : blindOnJoin
                })
            }

            setButton(currentPlayer)

            store.set('currentPlayer', currentPlayer)
            this.setState({currentPlayer: currentPlayer})


            // on initial connection, force the player to bet big/small blind
            function blindOnJoin(){
                let bet = 0;

                if (table.pot === table.bigBlind){
                    // second player gets small blind, even if the big blind player has left
                    if (table.players.length <= 1) bet = table.smallBlind
                } else {
                    // first player gets big blind if it hasn't been given away yet
                    if (table.players.length === 0 && table.pot !== (table.bigBlind + table.smallBlind)) bet = table.bigBlind
                }

                bet && Meteor.call('bet', table._id, currentPlayer.id, bet, error => error && console.log(error))
            }

            // set the button in the UI
            function setButton(player){
                player.bigBlind = table.players.length === 1
                player.smallBlind = false
                player.dealer = false

                if (table.players.length > 1){
                    const playerId = player.id._str

                    // set big blind
                    const bigBlindIndex = table.bigBlindIndex
                    const bigBlindPlayerId = table.players[bigBlindIndex].id._str
                    player.bigBlind = playerId === bigBlindPlayerId

                    // set small blind
                    let smallBlindIndex = (table.bigBlindIndex + 1) % table.players.length
                    for (let sb = smallBlindIndex;sb !== table.bigBlindIndex;++sb < table.players.length || (sb = 0)){
                        if ((player = table.players[sb]).chips > 0 || player.bet > 0){
                            smallBlindPlayerId = player.id._str
                            player.smallBlind = playerId === smallBlindPlayerId
                            smallBlindIndex = sb
                            break
                        }
                    }

                    // set dealer
                    if (table.players.filter(p => p.chips > 0 || p.bet > 0).length > 2){
                        const dealerIndex = (smallBlindIndex + 1) % table.players.length
                        for (let d = dealerIndex;d !== table.bigBlindIndex;++d < table.players.length || (d = 0)){
                            if ((player = table.players[d]).chips > 0 || player.bet > 0){
                                dealerPlayerId = player.id._str
                                player.dealer = playerId === dealerPlayerId
                                break
                            }
                        }
                    } else {
                        // in a 2 player match, the small blind is the dealer
                        player.dealer = player.smallBlind
                    }
                }
            }
        }
    }

    toggleFullName(){
        const element = this.refs.tableName;
        if (element.scrollWidth <= element.clientWidth && !this.state.fullTableName) return;

        const currentlyDisplayed = !!this.state.fullTableName;
        this.setState({fullTableName: !currentlyDisplayed})
    }

    render(){
        return (
            <div className="table-felt full-height">
                <div className="z-depth-2">
                    <nav>
                        <div className="nav-wrapper">
                            <span ref='tableName' onClick={this.toggleFullName.bind(this)} className={`brand-logo center table-name ${this.state.fullTableName && 'table-name-full'}`}>{this.state.displayUserSettings ? 'User Settings' : (this.props.table ? this.props.table.name : '')}</span>

                            <ul id="nav-mobile" className="right">
                                <li><a onClick={this.openUserSettings.bind(this)}><i className="material-icons">{this.state.displayUserSettings ? 'close' : 'perm_identity'}</i></a></li>
                            </ul>

                            <ul id="nav-mobile" className="left">
                                <li><a onClick={this.promptExitRoom.bind(this)}><i className="material-icons">skip_previous</i></a></li>
                            </ul>
                        </div>
                    </nav>

                    <PlayersList players={this.props.table.players} table={this.props.table} />

                    <UserSettings table={this.props.table} player={this.state.currentPlayer} display={this.state.displayUserSettings} />
                </div>

                <Button player={this.state.currentPlayer} />

                <div className="show-hands">
                    <a className="waves-effect waves-light btn-flat white-text modal-trigger" onClick={this.displayHandsRanking.bind(this)}>
                       Hands <i className="material-icons">sort</i>
                    </a>
                </div>

                <ActionsController displayUserSettings={this.state.displayUserSettings} table={this.props.table} player={this.state.currentPlayer} />

                <div id="modal-leave" className="modal black-text">
                    <div className="modal-content">
                        <h4>Leave Room</h4>
                        <p>Are you sure you want to leave this room ?</p>
                    </div>
                    <div className="modal-footer">
                        <a onClick={this.exitRoom.bind(this)} className="modal-action modal-close waves-effect waves-green btn-flat">Yes</a>
                        <a onClick={() => setTimeout(() => $('.lean-overlay').remove(), 120)} className="modal-action modal-close waves-effect waves-red btn-flat">No</a>
                    </div>
                </div>

                <div id="modal-hands" className="modal black-text">
                    <img className="full-width-modal-img" src="img/hands.png" alt="poker hands ranking" />
                </div>
            </div>
        );
    }

    openUserSettings(){ this.setState({displayUserSettings : !this.state.displayUserSettings}) }
    promptExitRoom(){ $('#modal-leave').modal('open') }
    displayHandsRanking(){ $('#modal-hands').modal('open') }

    exitRoom(){
        Meteor.call('tables.leave', this.props.table._id, store.get('currentPlayer'))
        $('.lean-overlay').remove();
        store.remove('room');
        this.context.router.push('/');
    }
}

Room.contextTypes = {
    router: PropTypes.object
}

Room.propTypes = {
    table: PropTypes.object
}

Room.defaultProps = {
    table: { players: [] }
}

export default createContainer(params => {
    const tableIdString = params.routeParams.tableId;
    store.set('room', tableIdString);

    // requires "idGeneration: 'MONGO'"
    const tableId = new Meteor.Collection.ObjectID(tableIdString);

    return {
        table: Tables.findOne(tableId)
    }
}, Room);