/*
* calculate the split pots associated to multiple players going all-in
*
* @param players {array} list of players and their total bet [{totalBet: 0, player: x}]
* @returns {array} list of sidepots with their players [{pot: 0, players: [{}]}]
*/
export function calculateSidePots(players){
  if (players.length < 2) return console.error('There has to be at least two players to calculate side-pots.')

  const sidePots = []

  players
    // sort from lowest to highest
    .sort((a, b) => a.totalBet - b.totalBet)

    // distribute side pots
    .forEach((player, currentPlayerIndex) => {
      let totalBet = player.totalBet

      // no side pot for the first player that goes all-in
      if (currentPlayerIndex === 0) return

      // the last player to go all-in doesn't create a side pot
      for (let i = currentPlayerIndex - 1;i > 1;i--){
        totalBet -= players[i].totalBet - players[i - 1].totalBet
      }

      // everyone bets in the main pot
      totalBet -= players[0].totalBet

      // add current player to all existing sidepots
      sidePots.forEach(({players}) => players.push(player))

      // the side pot is equal to the amount of players in the pot * the minimum bet
      let sidePot = {pot: (players.length - currentPlayerIndex) * totalBet, players: [player]}

      // do not push empty sidepot
      sidePot.pot && sidePots.push(sidePot)
    })

  return sidePots
}