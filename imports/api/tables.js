import {Mongo} from 'meteor/mongo';
import {calculateSidePots} from './business'

export const Tables = new Mongo.Collection('tables', {
    idGeneration: 'MONGO'
});

if (Meteor.isServer){
	Meteor.publish('joinTable', function joinTable(id, player){
		joinedTable = Tables.update(
            {_id: id},
            {$addToSet: {players: player}},
            () => this.ready()
        );
	})
}

Meteor.methods({
    'tables.create'(table){
        return Tables.insert(table);
    },

    'tables.leave'(tableId, {id}){
        // remove player from table with "tableId"
        Tables.update(
            {_id: tableId},
            {$pull: {players: {id: id}}}
        )

        // remove tables over a day old
        var yesterday = new Date(new Date() - 86400000)
        Tables.remove(
            {createdAt: {$lte: yesterday}}
        )
    },

	'tables.updatePlayerName'(tableId, playerId, newName){
		Tables.update(
			{_id: tableId, 'players.id': playerId},
			{ $set: { 'players.$.name': newName }}
		)
	},

    'tables.updatePlayerChips'(tableId, playerId, chips){
        Tables.update(
            {_id: tableId, 'players.id': playerId},
            { $set: { 'players.$.chips': chips ? parseInt(chips) : 0 }}
        )
    },

    'tables.nextTurn'(tableId){
        const table = Tables.findOne(tableId)

        // reset all player's bets
        table.players.forEach(p => p.bet = 0)

        // go to next turn
        table.turn = (table.turn || 0) + 1

        Tables.update(tableId, table)
    },

    'tables.win'(tableId, ...winnerIds){
        const table = Tables.findOne(tableId)
        const tablePlayers = table.players
        const winnerIdStrings = winnerIds.map(w => w._str)

        function isWinnerInASidePot(){
            return table.allIns && table.allIns
                // minimum all-in is in the main pot, not a side pot
                .filter(({totalBet}) => totalBet != Math.min(...table.allIns.map(a => a.totalBet)))
                .some(({player}) => ~winnerIdStrings.indexOf(player._str))
        }

        // distribute pot
        if (winnerIds.length === 1 || !isWinnerInASidePot()){
            tablePlayers
                .filter(({id}) => ~winnerIdStrings.indexOf(id._str))
                .forEach(p => p.chips += table.pot / winnerIds.length)
        } else {
            // all-ins
            const sidePots = calculateSidePots(table.allIns)

            sidePots.forEach(({pot, players}) => {
                let winnersInThisPot = players.filter(({_str}) => ~winnerIds.map(w => w._str).indexOf(_str))
                let splitCount = winnersInThisPot.length

                if (splitCount){
                    tablePlayers
                        .filter(({id}) => ~winnerIds.map(w => w._str).indexOf(id._str))
                        .forEach(p => p.chips += pot / splitCount)
                } else {
                    // this side pot hasn't been won, distribute it to each player
                    tablePlayers
                        .filter(({id}) => ~players.map(({player}) => player._str).indexOf(id._str))
                        .forEach(p => p.chips += pot / players.length)
                }
            })
        }

        // reset players
        tablePlayers.forEach(p => {
            p.bet = 0
            p.folded = p.chips === 0
            p.totalBet = 0
        })

        // reset the table
        table.pot = 0
        table.turn = 0
        table.idsRequestingTie = []
        table.allIns = []

        // set big blind
        table.bigBlindIndex = ++table.bigBlindIndex % tablePlayers.length
        let newBigBlindIndex = table.bigBlindIndex
        do {
            if ((player = tablePlayers[newBigBlindIndex]).chips > 0){
                player.bet = table.bigBlind
                player.chips -= table.bigBlind
                table.pot += table.bigBlind
                break
            }

            ++newBigBlindIndex < tablePlayers.length || (newBigBlindIndex = 0)
        } while (newBigBlindIndex !== table.bigBlindIndex)
        table.bigBlindIndex = newBigBlindIndex

        // set big blind
        let smallBlindIndex = (table.bigBlindIndex + 1) % tablePlayers.length
        while (smallBlindIndex !== table.bigBlindIndex){
            if ((player = tablePlayers[smallBlindIndex]).chips > 0){
                player.bet = table.smallBlind
                player.chips -= table.smallBlind
                table.pot += table.smallBlind
                break
            }

            ++smallBlindIndex < tablePlayers.length || (smallBlindIndex = 0)
        }

        return Tables.update(tableId, table)
    },

    bet(tableId, playerId, amount){
        return Tables.update(
            {_id: tableId, 'players.id': playerId},
            {
                $inc: {
                    'players.$.bet': amount,
                    'players.$.totalBet': amount,
                    'players.$.chips': -amount,
                    'pot': amount,
                }
            }
        )
    },

    allIn(tableId, playerId, amount, playerTotalBet){
        return Tables.update(
            {_id: tableId, 'players.id': playerId},
            {
                $push: {
                    allIns: {player: playerId, totalBet: playerTotalBet}
                },
                $inc: {
                    'players.$.bet': amount,
                    'players.$.totalBet': amount,
                    'players.$.chips': -amount,
                    'pot': amount,
                }
            }
        )
    },

    fold(tableId, playerId){
        return Tables.update(
            {_id: tableId, 'players.id': playerId},
            {$set: {'players.$.folded': true}}
        )
    },

    requestTie(tableId, playerId, cancel = false){
        const request = cancel ? -1 : 1

        return Tables.update(
            {_id: tableId},
            {$addToSet: {'idsRequestingTie': playerId}}
        )
    }
});