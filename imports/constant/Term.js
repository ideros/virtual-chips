const PRE_FLOP 	= 0;
const FLOP 		  = 1;
const TURN 		  = 2;
const RIVER		  = 3;

export default class Term {
	static get PRE_FLOP()	{ return PRE_FLOP }
	static get FLOP()		  { return FLOP }
	static get TURN()		  { return TURN }
	static get RIVER()		{ return RIVER }
}