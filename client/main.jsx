import React from 'react';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import {Meteor} from 'meteor/meteor';
import {render} from 'react-dom';

import ListTables from '../imports/ui/login/ListTables';
import Room from '../imports/ui/Room';

Meteor.startup(() => {
  render(<Router history={hashHistory}>
    <Route path="/" component={ListTables}></Route>
    <Route path=":tableId" component={Room}></Route>
  </Router>, 
  document.getElementById('render-target'));
})