# README #

A Meteor/ReactJS web application that allows you to play poker with only a pack of cards.
![virtual chips](https://rlv.zcache.com/carbon_fiber_any_color_poker_chips_set-r2b608122bb2846e8a24dfdb0282f97b5_zrag1_8byvr_630.jpg?view_padding=%5B285%2C0%2C285%2C0%5D)

### Getting Started
1. clone this repo `git clone git@bitbucket.org:ideros/virtual-chips.git`
2. install meteor https://www.meteor.com/install
3. install project dependencies using `meteor npm install`
4. start server `meteor`
5. navigate to your app at http://localhost:3000